<!doctype html>
<html lang="en">

<head>
    <title>Excello</title>
    <!-- Requiresd meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="../src/style.scss" type="text/scss">
</head>
<?php
    require('../lib/functions.php');
?>

<body>
    <div class="first">
    <nav class="font-weight-bold shadow-sm mb-1 navbar navbar-expand-lg navbar-dark lighten-1">
        <a class="navbar-brand" href="#">Excello</a>
        <button class="navbar-toggler bg-dark" type="button" data-toggle="collapse" data-target="#navbarSupportedContent-555"
            aria-controls="navbarSupportedContent-555" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent-555">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a class="nav-link" href="#">Acasa</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#about">Despre noi</a>
                </li>
            </ul>
        </div>
    </nav>

    <div class="container">
        <div class="row">
            <div class="col text-center">
                <h1 class="mt-5">Excello.</h1>
                <h4 class="mt-3">Transforma dieta ta</h4>
                <button class="mt-3 mb-3 font-weight-bold text-white btn btn-1"><a class="btn-detalii text-white"href="#detalii">Detalii</a></button>
                <button class="font-weight-bold text-white btn btn-2"><a class="btn-detalii text-white"href="#contact">Contact</a></button>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col">
                <img src="../dist/animal.jpg"class="shadow-lg mx-auto d-block">
            </div>
        </div>
    </div>
</div>

<?php
$result = queryResult('SELECT * from produse');
?>

<div class="second">
    <div class="container">
        <div class="row mt-5 text-center">
        <?php
      
    if ($result->num_rows > 0) {
      while($row = $result->fetch_assoc()) { 
   
        ?>
        <div class="col-md col-sm-12 col-12 mr-3 mt-2 mb-2 card">
            <a href="#" class="produse nav-link font-weight-bold"><?php echo $row["titlu"] ?></a>
            <p><?php echo $row["continut"] ?></p>
        </div>
      <?php   
      }
  }  else {
      `echo "0 results`;
    }
    ?>
    </div>
</div>

<div class="about mt-3">
    <h4 id="about" class='text-center'>DESPRE NOI</h4>
    <div class="row mt-5">
        <div class="col text-center">
            <img src="../dist/echipa.png"> 
            <h2>Echipa uimitoare!</h2>   
        </div>
        <div class="col text-center">
            <img src="../dist/munte.png">   
            <h2>Tintim sus!</h2> 
        </div>
        <div class="col text-center">
            <img src="../dist/contact.png">    
            <h2>Ia legatura cu noi!</h2>
        </div>
    </div>
</div>

<div class="row mt-3">
    <div id="detalii" class="col text-center jumbotron">
            <h4>Detalii</h4>
            <p class="mt-4">Firma noastra se ocupa de prelucrarea laptelui in favoarea clientului</p>
            <p>Produsele noastre sunt 100% naturale, si gata de distribuit in toata tara</p>
    </div>
    <div id="contact" class="col text-center jumbotron">
            <h4>Contact</h4>
            <p class="mt-4">excello@gmail.com</p>
            <p>+4073899428</p>
            <?php
    if(isset($_POST['submit'])) {
        $email = $_POST['email'];
        $mesaj = $email. ' a fost adugat cu succes!';
        $result = queryResult(
        "INSERT INTO contact (email)
        VALUES ('$email')
        ");
        if($result){
          echo "<script>alert('$mesaj');</script>"; 
        } else {
          echo 'A aparut o eroare';
        }
    }
?>
        <form action="index.php" method="post">
        <input required type="text" name="email" placeholder="Email..">
            <input class="btn bg-danger text-white font-weight-bold border mb-2" type="submit" name="submit" value="Te vom contacta!">
        </form>
    </div>
</div>

     <script src="bundle.js"></script>
</body>

</html>